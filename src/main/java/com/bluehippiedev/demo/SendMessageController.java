package com.bluehippiedev.demo;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.jms.JMSException;
import javax.jms.Topic;

@RestController
public class SendMessageController {

    private final JmsTemplate jmsTemplate;

    SendMessageController(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

//  curl -X GET http://localhost:8080/email\?message\=test
    @GetMapping("/email")
    public String sendMessage(@RequestParam("message") String message) throws JMSException {

        // Send a message with a POJO - the template reuse the message converter
        System.out.println("Sending an email message.");

        Topic mailBoxTopic = jmsTemplate.getConnectionFactory().createConnection().createSession().createTopic("mailbox");
        jmsTemplate.convertAndSend("mailbox", new Email("info@example.com", message));
        return "Message sent: " + message ;
    }

    /**
    curl -X POST http://localhost:8080/email \
     -H 'Content-Type: application/json' \
     -d '{"to":"info@example.com","body":"This is a message via POST"}'
     **/
    @PostMapping("/email")
    public String sendMessage(@RequestBody Email email) throws JMSException {
        System.out.println("Sending an email message.");
        System.out.println("#####: " + email);
        Topic mailBoxTopic = jmsTemplate.getConnectionFactory().createConnection().createSession().createTopic("mailbox");
        jmsTemplate.convertAndSend("mailbox", email);
        return "Post Message sent";
    }
}
