package com.bluehippiedev.demo;

import com.rabbitmq.jms.admin.RMQConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.amqp.RabbitProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.jms.ConnectionFactory;

@Configuration
public class RabbitConfiguration {

    private final RabbitProperties rabbitProperties;

    RabbitConfiguration(RabbitProperties properties) {
        this.rabbitProperties = properties;
    }

    @Bean
    ConnectionFactory connectionFactory() {
        RMQConnectionFactory connectionFactory = new RMQConnectionFactory();
        connectionFactory.setUsername(rabbitProperties.getUsername());
        connectionFactory.setPassword(rabbitProperties.getPassword());
        connectionFactory.setPort(rabbitProperties.determinePort());
        connectionFactory.setHost(rabbitProperties.determineHost());
        connectionFactory.setVirtualHost(rabbitProperties.determineVirtualHost());
        return connectionFactory;
    }
}
